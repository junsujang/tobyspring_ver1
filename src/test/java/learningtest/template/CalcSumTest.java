package learningtest.template;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CalcSumTest {
    private Calculator calculator;
    private String numFilePath;

    @Before
    public void setUp() throws IOException{
        this.calculator=new Calculator();
        this.numFilePath=new File("numbers.txt").getCanonicalPath();
    }

    @Test
    public void sumOfNumbers() throws IOException{
//        calculator = new Calculator();
//        int sum = calculator.calcSum(numFilePath);
        assertThat(calculator.calcSum(numFilePath),is(10));
    }

    @Test
    public void multiplyOfNumbers() throws IOException{
        assertThat(calculator.calcMultiply(this.numFilePath),is(24));
    }

    @Test
    public void concatenateStrings() throws IOException{
        assertThat(calculator.concatenate(this.numFilePath),is("1234"));
    }
}
