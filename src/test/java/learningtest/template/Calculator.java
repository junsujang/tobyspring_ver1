package learningtest.template;

import javax.sound.sampled.Line;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Calculator {

    public String concatenate(String filePath)throws IOException{
        LineCallback<String> concatenateCallBack = new LineCallback<String>(){
            public String doSomethingWithLine(String line, String value) {
                return value + line;
            }
        };
        return lineReadTemplate(filePath,concatenateCallBack,"");
    }

    public Integer calcMultiply(String filePath)throws IOException{
//        BufferedReaderCallback multiplyCallBack = new BufferedReaderCallback() {
//            public Integer doSomethingWithReader(BufferedReader bufferedReader) throws IOException {
//                Integer multiply = 1;
//                String line =null;
//                while((line = bufferedReader.readLine())!=null){
//                    multiply*=Integer.valueOf(line);
//                }
//                return multiply;
//            }
//        };
//        return fileReadTemplate(filePath,multiplyCallBack);

        LineCallback<Integer> multiplyLineCallBack = new LineCallback<Integer>() {
            public Integer doSomethingWithLine(String line, Integer value) {
                return value*Integer.valueOf(line);
            }
        };
        return lineReadTemplate(filePath,multiplyLineCallBack,1);
        //만약 파일의 각 라인에 있는 문자를 모두 연결해서 하나의 스트링으로 돌려주는 기능을 요청하면...?
    }

    public Integer calcSum(String filePath) throws IOException{
//        BufferedReader bufferedReader = null;
//        try {
//            bufferedReader = new BufferedReader(new FileReader(filePath));
//            Integer sum = 0;
//            String line = null;
//
//            while ((line = bufferedReader.readLine()) != null) {
//                sum += Integer.valueOf(line);
//            }
//            return sum;
//        }catch(IOException e){
//            System.out.println(e.getMessage());
//            throw e;
//        }finally {
//            if(bufferedReader!=null){
//                try{
//                    bufferedReader.close();
//                }catch(IOException e){
//                    System.out.println(e.getMessage());
//                }
//            }
//        }

//        BufferedReaderCallback sumCallBack = new BufferedReaderCallback() {
//            public Integer doSomethingWithReader(BufferedReader bufferedReader) throws IOException {
//                Integer sum = 0;
//                String line = null;
//
//                while ((line = bufferedReader.readLine()) != null) {
//                    sum += Integer.valueOf(line);
//                }
//                return sum;
//            }
//        };
//        return fileReadTemplate(filePath,sumCallBack);

        LineCallback<Integer> sumLineCallBack = new LineCallback<Integer>() {
            public Integer doSomethingWithLine(String line, Integer value) {
                return value+Integer.valueOf(line);
            }
        };
        return lineReadTemplate(filePath,sumLineCallBack,0);
    }

    public Integer fileReadTemplate(String filePath,BufferedReaderCallback callback)throws IOException{
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(filePath));
            // 콜백 오브젝트 호출. 템플릿에서 만든 컨텍스트 정보인 BufferedReader를 전달해주고 콜백의 작업 결과를 받아둔다.
            int result = callback.doSomethingWithReader(bufferedReader);
            return result;
        }catch(IOException e){
            System.out.println(e.getMessage());
            throw e;
        }finally {
            if(bufferedReader!=null){
                try{
                    bufferedReader.close();
                }catch(IOException e){
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    public <T> T lineReadTemplate(String filePath,LineCallback<T> callback,T initVal) throws IOException{
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(filePath));
            T res = initVal;
            String line = null;
            while((line=bufferedReader.readLine())!=null){
                res=callback.doSomethingWithLine(line,res);
            }
            return res;
        }catch(IOException e){
            System.out.println(e.getMessage());
            throw e;
        }finally {
            if(bufferedReader!=null){
                try{
                    bufferedReader.close();
                }catch(IOException e){
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}
