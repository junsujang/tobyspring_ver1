import com.junsu.toby.dao.UserDao;
import com.junsu.toby.dao.UserDaoJdbc;
import com.junsu.toby.dao.connection.NConnectionMaker;
import com.junsu.toby.domain.User;

import java.sql.SQLException;

public class NoJunitTest {
    public static void main(String[] args)throws ClassNotFoundException, SQLException {
        UserDao dao = new UserDaoJdbc(/*new NConnectionMaker()*/);

        User user = new User();
        user.setId("whiteship");
        user.setName("백기선");
        user.setPassword("married");

        dao.add(user);

        System.out.println(user.getId() + "등록 성공!");

        User user2 = dao.get(user.getId());
        System.out.println(user2.getName());
        System.out.println(user2.getPassword());

        System.out.println(user2.getId() + " 조회 성공");
    }
}
