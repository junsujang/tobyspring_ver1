import com.junsu.toby.dao.UserDao;
import com.junsu.toby.domain.Level;
import com.junsu.toby.domain.User;
import com.junsu.toby.service.UserLevelUpgradePolicy;
import com.junsu.toby.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.junsu.toby.service.UserLevelUpgradePolicy.MIN_LOGCOUNT_FOR_SILVER;
import static com.junsu.toby.service.UserLevelUpgradePolicy.MIN_RECOMMED_FOR_GOLD;
//import static com.junsu.toby.service.UserService.MIN_LOGCOUNT_FOR_SILVER;
//import static com.junsu.toby.service.UserService.MIN_RECOMMED_FOR_GOLD;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext.xml")
public class UserServiceTest {
    @Autowired
    UserService userService;
    @Autowired
    UserDao userDao;
    @Autowired
    DataSource dataSource;
    @Autowired
    UserLevelUpgradePolicy userLevelUpgradePolicy;
    @Autowired
    PlatformTransactionManager transactionManager;
    @Autowired
    MailSender mailSender;

    List<User> users;

    @Before
    public void setUp(){
        users = Arrays.asList(
                new User("a","a","a", Level.BASIC,MIN_LOGCOUNT_FOR_SILVER-1,0,"a@naver.com"),
                new User("b","b","b",Level.BASIC,MIN_LOGCOUNT_FOR_SILVER,0,"wnstn8372@gmail.com"),//로그인 50회 이상시 실버등급
                new User("c","c","c",Level.SILVER,60,MIN_RECOMMED_FOR_GOLD-1,"c@naver.com"),
                new User("d","d","d",Level.SILVER,60,MIN_RECOMMED_FOR_GOLD,"wnstn8372@naver.com"),//실버등급이면서 추천수가 30회이상시 골드등급
                new User("e","e","e",Level.GOLD,100,Integer.MAX_VALUE,"junsu.jang@architectgroup.com")
        );
    }

//    @Test
    public void bean(){
        assertThat(this.userService,is(notNullValue()));
    }

    static class MockMailSender implements MailSender{
         List<String> requests;

        public MockMailSender(){
            this.requests = new ArrayList<String>();
        }

        public List<String> getRequests(){
            return requests;
        }

        public void send(SimpleMailMessage simpleMailMessage) throws MailException {
            requests.add(simpleMailMessage.getTo()[0]);
        }

        public void send(SimpleMailMessage... simpleMailMessages) throws MailException {

        }
    }

    @Test
    @DirtiesContext// 추가된 메일 로직 테스트를 위해 @DirtiesContext를 기술함으로써 컨텍스트의 DI설정을 변경하는 테스트라는것을 알려줌
    public void upgradeLevels() throws Exception{
        userDao.deleteAll();
        for(User user:users){
            userDao.add(user);
        }

        // 메일 발송결과를 테스트할 수 있도록 목 오브젝트를 만들어 userService의 의존 오브젝트로 주입.
        MockMailSender mockMailSender = new MockMailSender();
        userService.setMailSender(mockMailSender);

        // [업그레이드 테스트]
        // 메일 발송이 일어나면 MockMailSender의 리스트에 결과가 저장.
        userService.upgradeLevels();

        checkLevelUpgraded(users.get(0),false);
        checkLevelUpgraded(users.get(1),true);
        checkLevelUpgraded(users.get(2),false);
        checkLevelUpgraded(users.get(3),true);
        checkLevelUpgraded(users.get(4),false);

        // 목오브젝트에 저자오딘 메일 수신자 목록을 가져와 업그레이드 대상과 일치하는지 확인.
        List<String> request = mockMailSender.getRequests();
        assertThat(request.size(),is(2));
        assertThat(request.get(0),is(users.get(1).getEmail()));
        assertThat(request.get(1),is(users.get(3).getEmail()));
    }

    @Test
    public void add(){
        userDao.deleteAll();

        User userWithLevel = users.get(4); //Gold 레벨
        User userWithoutLevel = users.get(0);
        userWithoutLevel.setLevel(null);

        userService.add(userWithLevel);
        userService.add(userWithoutLevel);

        User userWithLevelRead = userDao.get(userWithLevel.getId());
        User userWithoutLevelRead = userDao.get(userWithoutLevel.getId());

        assertThat(userWithLevelRead.getLevel(),is(userWithLevel.getLevel()));
        assertThat(userWithoutLevelRead.getLevel(),is(userWithoutLevel.getLevel()));
    }

    // 리팩토링 - 다음 레벨로 업그레이드에 대한 유무로 변경
    private void checkLevelUpgraded(User user,boolean upgraded){
        User userUpdate = userDao.get(user.getId());
        if(upgraded){
            // 다음레벨이 무엇인지는 Level에게 물어보면 된다. [user.getLevel().nextLevel()]
            assertThat(userUpdate.getLevel(),is(user.getLevel().nextLevel()));
        }else{
            assertThat(userUpdate.getLevel(),is(user.getLevel()));
        }
    }

    static class TestUserService extends UserService{
        private String id;

        private TestUserService(String id){
            this.id=id;
        }

        @Override
        protected void upgradeLevel(User user) {
            if(user.getId().equals(this.id)){
                throw new TestUserServiceException();
            }
            super.upgradeLevel(user);

        }
    }

    static class TestUserServiceException extends RuntimeException{

    }

    @Test
    public void upgradeAllOrNothing() throws Exception{
        UserService testUserService = new TestUserService(users.get(3).getId());
        testUserService.setUserDao(this.userDao);
        //추상화된 트랜잭션 -> DI를 활용한 리팩토링을 위해 주석처리
//        testUserService.setDataSource(dataSource);
        testUserService.setUserLevelUpgradePolicy(userLevelUpgradePolicy);
        testUserService.setTransactionManager(transactionManager);
        testUserService.setMailSender(mailSender);

        userDao.deleteAll();

        for(User user: users){
            userDao.add(user);
        }

        try{
            //TestUserService는 업그레이드 작업 중에 예외가 발생해야한다.정상종료라면 문제가 있으니 실패
            testUserService.upgradeLevels();
            fail("TestUserServiceException expected");
        }catch(TestUserServiceException e){
            //TestUserService가 던져주는 예외를 잡아서 계속 진행되도록 한다. 그외의 예외라면 테스트 실패
        }

        // 예외가 발생하기 전에 레벨 변경이 있었던 사용자의 레벨이 처음 상태로 바뀌었나 확인.
        checkLevelUpgraded(users.get(1),false);
    }

}
