import com.junsu.toby.dao.CountingDaoFactory;
import com.junsu.toby.dao.DaoFactory;
import com.junsu.toby.dao.UserDao;
import com.junsu.toby.dao.connection.ConnectionMaker;
import com.junsu.toby.dao.connection.CountingConnectionMaker;
import com.junsu.toby.dao.connection.DConnectionMaker;
import com.junsu.toby.domain.Level;
import com.junsu.toby.domain.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

// 만약, DataSource가 서버의 DB 풀 서비스와 연결해서 운영용 DB커넥션을 돌려주도록 만들어져있다고하면 테스트할때 DataSource를 이용해도 될까?
// UserDaoTest를 실행하는 순간 deleteAll()에 의해 운영용 DB의 사용자 정보가 모두 삭제된다면? -> setUp() DataSource오브젝트를 직접 생성 + 상태변경사실을 알림
//테스트 메소드에서 ApplicationContext의 구성이나 상태를 변경한다는 것을 테스트 컨텍스트 프레임워크에 알려준다.
//@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext.xml")
public class UserDaoTest {
//    @Autowired
//    private ApplicationContext context;

    @Autowired
    private UserDao dao;

    private User user1;
    private User user2;
    private User user3;

    @Before
    public void setUp(){
        // 매번 테스트마다 ApplicationContext객체를 생성하는데 시간을 잡아먹음.(싱글톤레지스트리라 하더라도 생성하는데 시간을 주는 부분은 문제가 있음)
//        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

//        this.dao = context.getBean("userDao",UserDao.class);
//        System.out.println(this.context);
//        System.out.println(this);

        //테스트에서 UserDao가 사용할 DataSource오브젝트를 직접 생성한다.
//        DataSource dataSource = new SingleConnectionDataSource("jdbc:mysql://localhost/testdb","root","1234",true);
//        dao.setDataSource(dataSource);// 코드에 의한 수동 DI

        this.user1=new User("a","a","a", Level.BASIC,1,0,"wnstn8372@naver.com");
        this.user2=new User("b","b","b",Level.SILVER,55,10,"wnstn8372@gmail.com");
        this.user3=new User("c","c","c",Level.GOLD,100,40,"junsu.jang@architectgroup.com");
    }

    @Test
    public void addAndGet() throws SQLException,ClassNotFoundException{
//        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
//        UserDao dao = context.getBean("userDao",UserDao.class);

        dao.deleteAll();
        assertThat(dao.getCount(),is(0));

//        User user = new User("wnstn8372","1234","junsu");
//        User user2 = new User("a","a","a");

        dao.add(user1);
        dao.add(user2);
        assertThat(dao.getCount(),is(2));


        User userget1 = dao.get(user1.getId());
        checkSameUser(userget1,user1);

        assertThat(userget1.getName(),is(user1.getName()));
        assertThat(userget1.getPassword(),is(user1.getPassword()));

        User userget2 = dao.get(user2.getId());
        checkSameUser(userget2,user2);

        assertThat(userget2.getName(),is(user2.getName()));
        assertThat(userget2.getPassword(),is(user2.getPassword()));
    }

    @Test
    public void count() throws SQLException,ClassNotFoundException{
//        ApplicationContext context = new GenericXmlApplicationContext("applicationContext.xml");
//        UserDao dao = context.getBean("userDao",UserDao.class);

        this.user1 = new User("a","a","a", Level.BASIC,1,0,"wnstn8372@naver.com");
        this.user2 = new User("b","b","b",Level.SILVER,55,10,"wnstn8372@gmail.com");
        this.user3 = new User("c","c","c",Level.GOLD,100,40,"junsu.jang@architectgroup.com");

        dao.deleteAll();
        assertThat(dao.getCount(),is(0));

        dao.add(user1);
        assertThat(dao.getCount(),is(1));

        dao.add(user2);
        assertThat(dao.getCount(),is(2));

        dao.add(user3);
        assertThat(dao.getCount(),is(3));
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void getUserFailure() throws SQLException, ClassNotFoundException{
//        ApplicationContext context = new GenericXmlApplicationContext("applicationContext.xml");
//        UserDao dao = context.getBean("userDao",UserDao.class);

        dao.deleteAll();
        assertThat(dao.getCount(),is(0));

        dao.get("uasdf");
    }

    @Test
    public void getAll() throws SQLException,ClassNotFoundException{
        dao.deleteAll();

        //데이터가 없을 때는 크기가 0인 리스트 오브젝트가 리턴돼야 한다.
        List<User> users0 = dao.getAll();
        assertThat(users0.size(),is(0));


        dao.add(user1);
        List<User> users1 = dao.getAll();
        assertThat(users1.size(),is(1));
        checkSameUser(user1,users1.get(0));

        dao.add(user2);
        List<User> users2 = dao.getAll();
        assertThat(users2.size(),is(2));
        checkSameUser(user1,users2.get(0));
        checkSameUser(user2,users2.get(1));

        dao.add(user3);
        List<User> users3 = dao.getAll();
        assertThat(users3.size(),is(3));
        checkSameUser(user1,users3.get(0));
        checkSameUser(user2,users3.get(1));
        checkSameUser(user3,users3.get(2));
    }

    private void checkSameUser(User user1,User user2){
        assertThat(user1.getId(),is(user2.getId()));
        assertThat(user1.getName(),is(user2.getName()));
        assertThat(user1.getPassword(),is(user2.getPassword()));
        assertThat(user1.getLevel(),is(user2.getLevel()));
        assertThat(user1.getLogin(),is(user2.getLogin()));
        assertThat(user1.getRecommend(),is(user2.getRecommend()));
    }

    @Test(expected = DataAccessException.class)
    public void duplicateKey(){
        dao.deleteAll();

        dao.add(user1);
        dao.add(user1);
    }

    @Test
    public void update(){
        dao.deleteAll();

        // 수정하지 않아야할 대상의 내용이 그대로 남아있는지 확인이 불가능(where를 빼고도 테스트 success)
        // 수정할 사용자
        dao.add(user1);
        // 수정하지 않을 사용자
        dao.add(user2);

        user1.setName("장준수");
        user1.setPassword("wnstn0601");
        user1.setLevel(Level.GOLD);
        user1.setLogin(1000);
        user1.setRecommend(999);

        dao.update(user1);

        User user1update = dao.get(user1.getId());
        checkSameUser(user1,user1update);
        User user2same = dao.get(user2.getId());
        checkSameUser(user2,user2same);
    }


    /*public static void main(String[] args)throws ClassNotFoundException, SQLException {
        // Test 책임과 UserDao가 생성및 생성을위한 또다른 조건의 책임이 함께있음(책임을 분리할 필요가 있음)
//        ConnectionMaker connectionMaker = new DConnectionMaker();
//        UserDao dao = new UserDao(connectionMaker);

        // 생성과 관련된 책임을 분리(팩토리) -> Ioc
        UserDao daoFactory = new DaoFactory().userDao();

        // Spring Ioc 엔진을 이용해 DaoFactory -> Spring Config ApplicationContext로 변경
//        ApplicationContext context = new AnnotationConfigApplicationContext(DaoFactory.class);
        ApplicationContext context = new GenericXmlApplicationContext("applicationContext.xml");
        UserDao dao = context.getBean("userDao",UserDao.class);

        User user = new User();
        user.setId("whiteship9");
        user.setName("백기선9");
        user.setPassword("married9");

        dao.add(user);

        System.out.println(user.getId() + "등록 성공!");

        User user2 = dao.get(user.getId());
        System.out.println(user2.getName());
        System.out.println(user2.getPassword());

        System.out.println(user2.getId() + " 조회 성공");

        //===============================================
*//*
        UserDao daoFactory2 = new DaoFactory().userDao();

        //System.out.println(daoFactory.toString());
        //System.out.println(daoFactory2.toString());

        UserDao daoApplicationContext = context.getBean("userDao",UserDao.class);

        //System.out.println(daoApplicationContext.toString());
        //System.out.println(dao.toString());

        ApplicationContext context2 = new AnnotationConfigApplicationContext(CountingDaoFactory.class);

        UserDao dao2 = context2.getBean("userDao",UserDao.class);

        User userTest = new User();
        userTest.setId("whiteship7");
        userTest.setName("백기선7");
        userTest.setPassword("married7");

        dao2.add(userTest);

        System.out.println(userTest.getId() + "등록 성공!");

        User userTest2 = dao.get(userTest.getId());
        System.out.println(userTest2.getName());
        System.out.println(userTest2.getPassword());

        System.out.println(userTest2.getId() + " 조회 성공");

        CountingConnectionMaker ccm = context2.getBean("getConnection",CountingConnectionMaker.class);
        System.out.println("Connection counter : "+ccm.getCounter());
*//*
    }*/
}
