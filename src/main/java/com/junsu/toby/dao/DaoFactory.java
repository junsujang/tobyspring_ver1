package com.junsu.toby.dao;

import com.junsu.toby.dao.connection.ConnectionMaker;
import com.junsu.toby.dao.connection.DConnectionMaker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import javax.sql.DataSource;

@Configuration
public class DaoFactory {

    @Bean
    public DataSource dataSource(){
        SimpleDriverDataSource dataSource = new SimpleDriverDataSource();

        dataSource.setDriverClass(com.mysql.jdbc.Driver.class);
        dataSource.setUrl("jdbc:mysql://localhost/springbook");
        dataSource.setUsername("root");
        dataSource.setPassword("1234");

        return dataSource;
    }

    @Bean
    public ConnectionMaker getConnectionMaker(){
        return new DConnectionMaker();
    }

    @Bean
    public UserDao userDao(){
        // 다른 dao를 추가하게된다면 ConnectionMaker의 중복이 발생하여 helper메소드로 리펙토링.
//        ConnectionMaker connectionMaker = new DConnectionMaker();
        UserDao userDao = new UserDaoJdbc(/*getConnectionMaker()*/);
//        userDao.setConnectionMaker(getConnectionMaker());
//        userDao.setDataSource(dataSource());
        return userDao;
    }

    public MessageDao messageDao(){
        MessageDao messageDao = new MessageDao(getConnectionMaker());
        return messageDao;
    }

    public AccountDao accountDao(){
        AccountDao accountDao = new AccountDao(getConnectionMaker());
        return accountDao;
    }
}
