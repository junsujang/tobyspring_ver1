package com.junsu.toby.dao.connection;


import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class CountingConnectionMaker implements ConnectionMaker {
    private static final String DBNAME = "root";
    private static final String DBPW = "1234";

//    private ConnectionMaker realConnectionMaker;
    private DataSource dataSource;
    private int counter=0;

    public CountingConnectionMaker(DataSource dataSource){
        this.dataSource=dataSource;
    }

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        this.counter++;
        return dataSource.getConnection();
    }

    public int getCounter(){
        return this.counter;
    }
}
