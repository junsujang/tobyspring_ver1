package com.junsu.toby.dao;

import com.junsu.toby.Exception.DuplicateUserIdException;
import com.junsu.toby.dao.connection.ConnectionMaker;
import com.junsu.toby.dao.connection.DConnectionMaker;
import com.junsu.toby.dao.connection.SimpleConnectionMaker;
import com.junsu.toby.dao.template.AddStatement;
import com.junsu.toby.dao.template.DeleteAllStatement;
import com.junsu.toby.dao.template.JdbcContext;
import com.junsu.toby.dao.template.StatementStrategy;
import com.junsu.toby.domain.Level;
import com.junsu.toby.domain.User;
import com.mysql.jdbc.MysqlErrorNumbers;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;

public /*abstract*/ class UserDaoJdbc implements UserDao{
//    private static final String DBNAME = "root";
//    private static final String DBPW = "1234";

//    private SimpleConnectionMaker simpleConnectionMaker;
    //private ConnectionMaker connectionMaker;

//    private DataSource dataSource;

    private JdbcContext jdbcContext;

    private JdbcTemplate jdbcTemplate;

    // RowMapper 중복 제거
    private RowMapper<User> userMapper = new RowMapper<User>() {
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setId(rs.getString("id"));
            user.setName(rs.getString("name"));
            user.setPassword(rs.getString("password"));
            user.setLevel(Level.valueOf(rs.getInt("level")));
            user.setLogin(rs.getInt("login"));
            user.setRecommend(rs.getInt("recommend"));
            return user;
        }
    };

//    public void setJdbcContext(JdbcContext jdbcContext){
//        this.jdbcContext=jdbcContext;
//    }

    // 수정자 메소드이면서 jdbcContext에 대한 생성. DI작업을 동시에 수행.
    public void setDataSource(DataSource dataSource) {
        this.jdbcContext=new JdbcContext();// jdbcContext 생성(Ioc)
        this.jdbcContext.setDataSource(dataSource);//의존 오브젝트 주입(DI)

        this.jdbcTemplate=new JdbcTemplate(dataSource);

        // 아직 jdbcContext를 적용하지 않은 메소드를 위해 저장.
//        this.dataSource = dataSource;
    }

    //public UserDaoJdbc(ConnectionMaker connectionMaker){
        //simpleConnectionMaker = new SimpleConnectionMaker();
        //this.connectionMaker = new DConnectionMaker();
        //this.connectionMaker = connectionMaker;
    //}

    /*public void setConnectionMaker(ConnectionMaker connectionMaker){
        this.connectionMaker=connectionMaker;
    }*/

    //public abstract Connection getConnection() throws ClassNotFoundException,SQLException;

//    public Connection getConnection() throws ClassNotFoundException,SQLException{
//        Class.forName("com.mysql.jdbc.Driver");
//        Connection con = DriverManager.getConnection("jdbc:mysql://localhost/springbook",DBNAME,DBPW);
//        return con;
//    }

    public void add(final User user) throws DuplicateUserIdException {
        //Connection con = getConnection();

//        Connection con = simpleConnectionMaker.makeNewConnection();

//        Connection con = connectionMaker.getConnection();
//        Connection con = null;
//        PreparedStatement ps = null;
//
//        try {
//            con = dataSource.getConnection();
//
//            ps = con.prepareStatement("insert into users(id,name,password) values(?,?,?)");
//            ps.setString(1, user.getId());
//            ps.setString(2, user.getName());
//            ps.setString(3, user.getPassword());
//            ps.executeUpdate();
//        }catch(SQLException e){
//            throw e;
//        }finally {
//            ps.close();
//            con.close();
//        }
        // 2가지의 문제
        // 1. dao메소드마다 새로운 StatementStrategy 구현 클래스를 만들어야하는점.(파일 개수 Up)
        // 2. StatementStrategy 구현 클래스에 부가적인 정보(add() -> User class)를 넘겨야할경우(넘겨받을 생성자와 저장할 전역변수필요)
//        StatementStrategy strategy = new AddStatement();
//        jdbcContextWithStatementStrategy(strategy);

        // 따라서, 해결책은 어차피 한 메소드에서만 쓰이기에 내부클래스로 정의 (2가지의 문제 해결)
//        class AddStatement implements StatementStrategy {
//            public PreparedStatement makePreparedStatement(Connection con) throws SQLException {
//                PreparedStatement ps = con.prepareStatement("insert into users(id,name,password) values(?,?,?)");
//                ps.setString(1, user.getId());
//                ps.setString(2, user.getName());
//                ps.setString(3, user.getPassword());
//
//                return ps;
//            }
//        }

        // 내부클래스의 종류중 하나인 익명클래스를 이용하여 깔끔하게 코드구성 가능.
//        StatementStrategy strategy = new StatementStrategy() {
//            public PreparedStatement makePreparedStatement(Connection con) throws SQLException {
//                PreparedStatement ps = con.prepareStatement("insert into users(id,name,password) values(?,?,?)");
//                ps.setString(1, user.getId());
//                ps.setString(2, user.getName());
//                ps.setString(3, user.getPassword());
//
//                return ps;
//            }
//        };

//        jdbcContextWithStatementStrategy(new StatementStrategy() {
//            public PreparedStatement makePreparedStatement(Connection con) throws SQLException {
//                PreparedStatement ps = con.prepareStatement("insert into users(id,name,password) values(?,?,?)");
//                ps.setString(1, user.getId());
//                ps.setString(2, user.getName());
//                ps.setString(3, user.getPassword());
//
//                return ps;
//            }
//        });

        // jdbcContext를 만들어서 어떤 dao던 간에 쓸수있도록 재구성(리펙토링)
//        this.jdbcContext.workWithStatamentStrategy(new StatementStrategy() {
//            public PreparedStatement makePreparedStatement(Connection con) throws SQLException {
//                PreparedStatement ps = con.prepareStatement("insert into users(id,name,password) values(?,?,?)");
//                ps.setString(1, user.getId());
//                ps.setString(2, user.getName());
//                ps.setString(3, user.getPassword());
//
//                return ps;
//            }
//        });

        this.jdbcTemplate.update("insert into users(id,name,password,level,login,recommend,email) values(?,?,?,?,?,?,?)",
                user.getId(),user.getName(),user.getPassword(),
                user.getLevel().intValue(),user.getLogin(),user.getRecommend(),user.getEmail());

        // Exception 예외 전환
//        try{
//            // JDBC를 이용해 User 정보를 DB에 추가하는 코드
//            PreparedStatement ps = null;
//
//            ps.executeUpdate();
//        }catch(SQLException e){
//            //ErrorCode가 MySQL의 "Duplicate Entry(1062)"면 에외 전환
//            if(e.getErrorCode()== MysqlErrorNumbers.ER_DUP_ENTRY){
//                throw new DuplicateUserIdException(e);//예외 전환
//            }else{
//                throw new RuntimeException(e);//예외 포장
//            }
//        }
    }

    public User get(String id)/*throws ClassNotFoundException, SQLException*/{
        //Connection con = getConnection();

//        Connection con = simpleConnectionMaker.makeNewConnection();

//        Connection con = connectionMaker.getConnection();

//        Connection con = null;
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//
//        User user = null;
//
//        try {
//
//            con = dataSource.getConnection();
//
//            ps = con.prepareStatement("select * from users where id=?");
//            ps.setString(1, id);
//
//            rs = ps.executeQuery();
//
//            if (rs.next()) {
//                user = new User();
//                user.setId(rs.getString("id"));
//                user.setName(rs.getString("name"));
//                user.setPassword(rs.getString("password"));
//            }
//        }catch(SQLException e){
//            throw e;
//        }finally {
//            rs.close();
//            ps.close();
//            con.close();
//        }
//
//        if(user == null) throw new EmptyResultDataAccessException(1);
//
//        return user;

        return this.jdbcTemplate.queryForObject("select * from users where id = ?",
                new Object[]{id},
                this.userMapper);

    }

    public void deleteAll() /*throws SQLException*/{
//        Connection con = null;
//        PreparedStatement ps = null;
//        try{
//            con=dataSource.getConnection();
////            ps=con.prepareStatement("delete from users");
//            StatementStrategy strategy = new DeleteAllStatement();
//            ps = strategy.makePreparedStatement(con);
//
//            ps.executeUpdate();
//        }catch(SQLException e){
//            throw e;
//        }finally {
//            if(ps!=null) {
//                try {
//                    ps.close();
//                } catch (SQLException e) {}
//            }
//            if(con!=null){
//                try{
//                    con.close();
//                }catch(SQLException e){}
//            }
//        }


//        class DeleteAllStatement implements StatementStrategy {
//            public PreparedStatement makePreparedStatement(Connection con) throws SQLException {
//                PreparedStatement ps = con.prepareStatement("delete from users");
//                return ps;
//            }
//        }

        //선정한 전략 클래스의 오브젝트 생성
//        StatementStrategy strategy = new DeleteAllStatement();
        //컨텍스트 호출. 전략오브젝트 전달
//        jdbcContextWithStatementStrategy(strategy);

        // 익명클래스로 구성
//        jdbcContextWithStatementStrategy(new StatementStrategy() {
//            public PreparedStatement makePreparedStatement(Connection con) throws SQLException {
//                PreparedStatement ps = con.prepareStatement("delete from users");
//                return ps;
//            }
//        });

        // jdbcContext를 만들어서 어떤 dao던 간에 쓸수있도록 재구성(리펙토링)
//        this.jdbcContext.workWithStatamentStrategy(new StatementStrategy() {
//            public PreparedStatement makePreparedStatement(Connection con) throws SQLException {
//                PreparedStatement ps = con.prepareStatement("delete from users");
//                return ps;
//            }
//        });

        // 변하는것과 변하지 않는 것을 분리
        // Dao 책임내에서 모든 공통된 메소드가 같은 패턴으로 변하는것과 변하지 않는 것이 나온다면 이렇게 분리가 맞지만
        // 가독성 측면에서 보면 요리조리 찾아가며 확인해야하기에 가독성을 고려하면 분리를 안하는게 더 나은것 같음.
//        executeSql("delete from users");

        // UserDaoJdbc뿐만 아니라 여러 Dao들도 쓸수있도록 템플릿에 옮겨봄.
//        this.jdbcContext.executeSql("delete from users");

//        this.jdbcTemplate.update(new PreparedStatementCreator() {
//            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
//                return con.prepareStatement("delete from users");
//            }
//        });

        this.jdbcTemplate.update("delete from users");

    }

    public int getCount() /*throws SQLException*/{
//        Connection con = null;
//        PreparedStatement ps = null;
//        ResultSet rs =null;
//        int count = 0;
//
//        try {
//            con = dataSource.getConnection();
//
//            ps = con.prepareStatement("select count(*) from users");
//
//            rs = ps.executeQuery();
//            count = 0;
//            if (rs.next()) {
//                count = rs.getInt(1);
//            }
//        }catch(SQLException e){
//            throw e;
//        }finally {
//            if(rs!=null){
//                try{
//                    rs.close();
//                }catch(SQLException e){}
//            }
//            if(ps!=null) {
//                try {
//                    ps.close();
//                } catch (SQLException e) {}
//            }
//            if(con!=null){
//                try{
//                    con.close();
//                }catch(SQLException e){}
//            }
//        }
//        return count;

        return this.jdbcTemplate.query(new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                return con.prepareStatement("select count(*) from users");
            }
        }, new ResultSetExtractor<Integer>() {
            public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
                if(!rs.next()){
                    throw new SQLException("테이블이 존재하지않습니다.");
                }
                return rs.getInt(1);
            }
        });

    }

    public List<User> getAll(){
        return this.jdbcTemplate.query("select * from users order by id",
                this.userMapper);
    }

    public void update(User user) {
        this.jdbcTemplate.update("update users set name=?,password=?,level=?,login=?,recommend=?,email=? where id=?",
                user.getName(),user.getPassword(),user.getLevel().intValue(),user.getLogin(),user.getRecommend(),user.getEmail(),
                user.getId());
    }

    //    public void jdbcContextWithStatementStrategy(StatementStrategy stmt)throws SQLException{
//        Connection con =null;
//        PreparedStatement ps =null;
//
//        try{
//            con = dataSource.getConnection();
//
//            ps = stmt.makePreparedStatement(con);
//
//            ps.executeUpdate();
//        }catch (SQLException e){
//            throw e;
//        }finally {
//            if(ps!=null) {
//                try {
//                    ps.close();
//                } catch (SQLException e) {}
//            }
//            if(con!=null){
//                try{
//                    con.close();
//                }catch(SQLException e){}
//            }
//        }
//    }

    // UserDaoJdbc뿐만 아니라 여러 Dao들도 쓸수있도록 템플릿에 옮겨봄.
//    private void executeSql(final String query) throws SQLException{
//        this.jdbcContext.workWithStatamentStrategy(new StatementStrategy() {
//            public PreparedStatement makePreparedStatement(Connection con) throws SQLException {
//                PreparedStatement ps = con.prepareStatement(query);
//                return ps;
//            }
//        });
//    }
}
