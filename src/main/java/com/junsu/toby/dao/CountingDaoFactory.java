package com.junsu.toby.dao;

import com.junsu.toby.dao.connection.ConnectionMaker;
import com.junsu.toby.dao.connection.CountingConnectionMaker;
import com.junsu.toby.dao.connection.DConnectionMaker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import javax.sql.DataSource;

@Configuration
public class CountingDaoFactory {

    @Bean
    public UserDao userDao(){
        UserDao dao = new UserDaoJdbc();
//        dao.setConnectionMaker(getConnection());
        return dao;
    }



    @Bean
    public ConnectionMaker getConnection(){
        return new CountingConnectionMaker(dataSource());
    }

    @Bean
    public ConnectionMaker getRealConnection(){
        return new DConnectionMaker();
    }

    @Bean
    public DataSource dataSource(){
        SimpleDriverDataSource dataSource = new SimpleDriverDataSource();

        dataSource.setDriverClass(com.mysql.jdbc.Driver.class);
        dataSource.setUrl("jdbc:mysql://localhost/springbook");
        dataSource.setUsername("root");
        dataSource.setPassword("1234");

        return dataSource;
    }
}
