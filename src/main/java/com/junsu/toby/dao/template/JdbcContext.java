package com.junsu.toby.dao.template;


import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class JdbcContext {
    private DataSource dataSource;

    public void setDataSource(DataSource dataSource){
        this.dataSource=dataSource;
    }

    public void workWithStatamentStrategy(StatementStrategy strategy) throws SQLException{
        Connection con =null;
        PreparedStatement ps =null;

        try{
            con = this.dataSource.getConnection();

            ps = strategy.makePreparedStatement(con);

            ps.executeUpdate();
        }catch(SQLException e){
            throw e;
        }finally {
            if(ps!=null) {
                try {
                    ps.close();
                } catch (SQLException e) {}
            }
            if(con!=null){
                try{
                    con.close();
                }catch(SQLException e){}
            }
        }
    }

    public void executeSql(final String query)throws SQLException{
        workWithStatamentStrategy(new StatementStrategy() {
            public PreparedStatement makePreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(query);
                return ps;
            }
        });
    }
}
