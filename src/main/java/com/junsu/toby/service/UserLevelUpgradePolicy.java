package com.junsu.toby.service;

import com.junsu.toby.domain.User;

// 연말 이벤트나 새로운 서비스 홍보기간 중에는 레벨 업그레이드 정책을 다르게 적용할 필요가 있을수있음.
// 이벤트기간동안 UserService를 직접 수정하면 끝난이후 복구하는데 상당히 위험
// 이런경우, 사용자 업그레이드 정책을 UserService에서 분리하는 방법을 고려
// 분리된 업그레이드 정책을 담은 오브젝트는 DI를 통해 UserService에 주입
// 업그레이드 정책을 담은 인터페이스를 만들어서 UserService는 DI로 제공받은 정책 구현 클래스를 이 인터페이스를 통해 사용
public interface UserLevelUpgradePolicy {
    int MIN_LOGCOUNT_FOR_SILVER=50;
    int MIN_RECOMMED_FOR_GOLD=30;

    boolean canUpgradeLevel(User user);
    void upgradeLevel(User user);
}
