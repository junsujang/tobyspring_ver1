package com.junsu.toby.service;

import com.junsu.toby.domain.Level;
import com.junsu.toby.domain.User;

public class UserLevelUpgradeDefault implements UserLevelUpgradePolicy{
    public boolean canUpgradeLevel(User user) {
        Level currentLevel = user.getLevel();
        switch (currentLevel){
            case BASIC:return (user.getLogin()>=MIN_LOGCOUNT_FOR_SILVER);
            case SILVER:return (user.getRecommend()>=MIN_RECOMMED_FOR_GOLD);
            case GOLD:return false;
            default:throw new IllegalArgumentException("Unknown Level: "+currentLevel);
        }
    }

    public void upgradeLevel(User user) {
        user.upgradeLevel();
    }
}
