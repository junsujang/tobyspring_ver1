package com.junsu.toby.service;

import com.junsu.toby.dao.UserDao;
import com.junsu.toby.domain.Level;
import com.junsu.toby.domain.User;
import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;
import com.sun.xml.internal.org.jvnet.mimepull.MIMEMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import sun.rmi.transport.Transport;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.io.UnsupportedEncodingException;
import java.nio.channels.UnsupportedAddressTypeException;
import java.sql.Connection;
import java.util.List;
import java.util.Properties;

public class UserService {

//    public static final int MIN_LOGCOUNT_FOR_SILVER=50;
//    public static final int MIN_RECOMMED_FOR_GOLD=30;

    UserDao userDao;
    UserLevelUpgradePolicy userLevelUpgradePolicy;

    // PlatformTransactionManager를 DI받으면서 DataSource도 property로 DI받을예정이기에 주석처리
//    private DataSource dataSource;

    PlatformTransactionManager transactionManager;

    private MailSender mailSender;

//    public void setDataSource(DataSource dataSource) {
//        this.dataSource = dataSource;
//    }

    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void setTransactionManager(PlatformTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    public void setUserLevelUpgradePolicy(UserLevelUpgradePolicy userLevelUpgradePolicy) {
        this.userLevelUpgradePolicy = userLevelUpgradePolicy;
    }

    public void setUserDao(UserDao userDao){
        this.userDao=userDao;
    }

    public void upgradeLevels() throws Exception{
    /*
        // 트랜젝션 동기화 관리자를 이용해 동기화 작업을 초기화한다.
        TransactionSynchronizationManager.initSynchronization();
        // DB Connection 생성과 동기화를 함께 해주는 유틸리티 메소드
        Connection con = DataSourceUtils.getConnection(dataSource);
        con.setAutoCommit(false);
        try {
            List<User> users = userDao.getAll();
            for (User user : users) {
                if (canUpgradeLevel(user)) {
                    upgradeLevel(user);
                }
            }
            con.commit();
        }catch(Exception e){
            con.rollback();
            throw e;
        }finally {
            DataSourceUtils.releaseConnection(con,dataSource);
            // 동기화 작업 종료 및 정리
            TransactionSynchronizationManager.unbindResource(this.dataSource);
            TransactionSynchronizationManager.clearSynchronization();
        }
    */

        //DB 연결종류별로 Transaction 처리 로직이 다름
        //현재 트랜잭션 처리 로직은 JDBC로 한정
        //만약 고객사별로 분산 DB로 연결되는 종류도 다르면..? 하이버네이트로 되어있는데 JDBC와 하이버네이트의 트랜잭션 로직이 다르면?
        //추상화가 필요(Spring에서 지원)
    /*
        PlatformTransactionManager transactionManager = new DataSourceTransactionManager(dataSource);// JDBC의 로컬 트랜잭션 이용.
        TransactionStatus status = transactionManager.getTransaction(new DefaultTransactionDefinition());
        try{
            List<User> users = userDao.getAll();
            for (User user : users) {
                if (canUpgradeLevel(user)) {
                    upgradeLevel(user);
                }
            }
            transactionManager.commit(status);
        }catch(RuntimeException e){
            transactionManager.rollback(status);
            throw e;
        }
    */
        //DI를 활용한 리팩토링된 로직
        TransactionStatus status = this.transactionManager.getTransaction(new DefaultTransactionDefinition());
        try{
            List<User> users = userDao.getAll();
            for (User user : users) {
                if (canUpgradeLevel(user)) {
                    upgradeLevel(user);
                }
            }
            this.transactionManager.commit(status);
        }catch(RuntimeException e){
            this.transactionManager.rollback(status);
            throw e;
        }
    }

    protected boolean canUpgradeLevel(User user){
//        Level currentLevel = user.getLevel();
//        switch (currentLevel){
//            case BASIC:return (user.getLogin()>=MIN_LOGCOUNT_FOR_SILVER);
//            case SILVER:return (user.getRecommend()>=MIN_RECOMMED_FOR_GOLD);
//            case GOLD:return false;
//            default:throw new IllegalArgumentException("Unknown Level: "+currentLevel);
//        }
        return this.userLevelUpgradePolicy.canUpgradeLevel(user);
    }

    // 테스트를 위해 접근지정자 변경
    protected void upgradeLevel(User user){
//        if(user.getLevel()==Level.BASIC){
//            user.setLevel(Level.SILVER);
//        }else if(user.getLevel()==Level.SILVER){
//            user.setLevel(Level.GOLD);
//        }
        //업그레이드가 불가능한 레벨에 대해서 Exception처리를 upgradeLevel 메소드에서 처리함.
//        user.upgradeLevel();
        this.userLevelUpgradePolicy.upgradeLevel(user);
        userDao.update(user);
        sendUpgradeEmail(user);

        //다음 단계가 무엇인가 하는 로직과 그때 사용자 오브젝트의 level 필드를 변경해준다는 로직이 함께 있음.
        //예외상황에 대한 처리가 없음.
        //레벨이 늘어나면, if문도 길어지며, 조건또한 내용이 길어진다.
        //다음단계에 대한 책임을 Level enum에게 전가!
        //사용자 정보가 바뀌는 로직을 User에게 책임을 지게 함.(user.upgradeLevel() 메소드)
    }

    private void sendUpgradeEmail(User user){
//        Properties props = new Properties();
//        props.put("mail.smtp.host","mail.ksug.org");
//        Session session = Session.getInstance(props,null);
//
//        MIMEMessage message = new MIMEMessage(session);
//        try{
//            message.setFrom(new InternetAddress("wnstn8372@naver.com"));
//            message.addRecipient(Message.RecipientType.TO,new InternetAddress(user.getEmail()));
//            message.setSubject("Upgrade 안내");
//            message.setText("사용자님의 등급이 "+user.getLevel().name()+"로 업그레이드되었습니다.");
//
//            Transport.send(message);
//        }catch(UnsupportedAddressTypeException e){
//            throw new RuntimeException(e);
//        }catch(MessagingException e){
//            throw new RuntimeException(e);
//        }catch(UnsupportedEncodingException e){
//            throw new RuntimeException(e);
//        }
        // JavaMail은 확장이나 지원이 불가능하도록 만들어진 가장 악명 높은 표준 api다.

        //Spring에서 제공
//        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
//        mailSender.setHost("mail.server.com");

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getEmail());
        mailMessage.setFrom("wnstn8372@naver.com");
        mailMessage.setSubject("Upgrade 안내");
        mailMessage.setText("사용자님의 등급이 "+user.getLevel().name());

        this.mailSender.send(mailMessage);
    }

    public void add(User user) {
        if(user.getLevel()==null) user.setLevel(Level.BASIC);
        userDao.add(user);
    }
}
