package com.junsu.toby.domain;

public enum Level {
    GOLD(3, null), SILVER(2, GOLD), BASIC(1, SILVER);

    private final int value;
    private final Level next;

    //DB에 저장할 값을 넘겨줄 생성자를 만듬.
    Level(int value, Level next) {
        this.value = value;
        this.next = next;
    }

    public int intValue() {
        return value;
    }

    public Level nextLevel() {
        return this.next;
    }

    // 값으로부터 Level타입 오브젝트를 가져오도록 만든 스태틱 메소드
    public static Level valueOf(int value) {
        switch (value) {
            case 1:
                return BASIC;
            case 2:
                return SILVER;
            case 3:
                return GOLD;
            default:
                throw new AssertionError("UnKnown value: " + value);
        }
    }
}
